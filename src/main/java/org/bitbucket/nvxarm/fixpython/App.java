package org.bitbucket.nvxarm.fixpython;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        Arrays.stream(args).forEach(new Fixer()::fix);
    }
}
