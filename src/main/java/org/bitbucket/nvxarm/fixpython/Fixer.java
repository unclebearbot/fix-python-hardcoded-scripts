package org.bitbucket.nvxarm.fixpython;

import com.google.common.primitives.Bytes;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Fixer {
    private static final Logger log = LoggerFactory.getLogger(Fixer.class);

    public void fix(String pythonRootPath) {
        Python python = Optional.of(pythonRootPath)
                .map(Path::of)
                .map(Python::new)
                .orElseThrow();
        log.info("Python located at '{}'", python);
        File interpreter = python.getInterpreter();
        log.info("Interpreter detected at '{}'", interpreter);
        File[] scripts = python.getScripts();
        Arrays.stream(scripts).forEach(it -> update(it, interpreter));
        log.info("Completed");
    }

    private void update(File script, File interpreter) {
        try {
            File backup = File.createTempFile(script.getName() + '.', ".backup", script.getParentFile());
            FileUtils.copyFile(script, backup);
            File temp = File.createTempFile(script.getName() + '.', ".temp", script.getParentFile());
            FileUtils.copyFile(script, temp);
            modify(temp, interpreter);
            FileUtils.copyFile(temp, script);
            FileUtils.forceDelete(temp);
            log.info("Fixed '{}'", script);
        } catch (IOException e) {
            log.warn("Skipped '{}'", script);
            log.error(e.getMessage(), e);
        }
    }

    private void modify(File script, File interpreter) throws IOException {
        try (FileInputStream fileInputStream = new FileInputStream(script)) {
            byte[] fileBytes = fileInputStream.readAllBytes();
            fileInputStream.close();
            Pattern pattern = Pattern.compile("#!.+\\\\python\\.exe", Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(new String(fileBytes, StandardCharsets.UTF_8));
            if (!matcher.find()) {
                throw new IOException("Interpreter not found");
            }
            byte[] toReplace = matcher.group(matcher.groupCount()).getBytes(StandardCharsets.UTF_8);
            byte[] head = ArrayUtils.subarray(fileBytes, 0, Bytes.indexOf(fileBytes, toReplace));
            byte[] tail = ArrayUtils.subarray(fileBytes, head.length + toReplace.length, fileBytes.length);
            byte[] path = ("#!" + interpreter.getAbsolutePath().toLowerCase(Locale.ROOT)).getBytes(StandardCharsets.UTF_8);
            byte[] output = Bytes.concat(head, path, tail);
            try (FileOutputStream fileOutputStream = new FileOutputStream(script)) {
                fileOutputStream.write(output);
            }
        }
    }
}
