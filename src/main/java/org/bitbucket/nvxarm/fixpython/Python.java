package org.bitbucket.nvxarm.fixpython;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;

public class Python {
    private final Path path;

    public Python(Path path) {
        this.path = path;
    }

    public File getInterpreter() {
        return Optional.of(path)
                .map(Path::toFile)
                .filter(File::isDirectory)
                .filter(File::canRead)
                .map(File::listFiles)
                .map(Arrays::stream)
                .map(it -> it.filter(file -> file.isFile() && file.canRead() && StringUtils.equalsIgnoreCase(file.getName(), "python.exe")).findFirst().orElseThrow())
                .orElseThrow();
    }

    public File[] getScripts() {
        return Optional.of(path)
                .map(Path::toFile)
                .filter(File::isDirectory)
                .filter(File::canRead)
                .map(File::listFiles)
                .map(Arrays::stream)
                .map(it1 -> it1.filter(file -> file.isDirectory() && file.canRead() && file.canWrite() && StringUtils.equalsIgnoreCase(file.getName(), "scripts")).findFirst().orElseThrow())
                .orElseThrow()
                .listFiles(it -> it.isFile() && it.canRead() && it.canWrite() && StringUtils.endsWithIgnoreCase(it.getName(), ".exe"));
    }

    @Override
    public String toString() {
        return path.toString();
    }
}
