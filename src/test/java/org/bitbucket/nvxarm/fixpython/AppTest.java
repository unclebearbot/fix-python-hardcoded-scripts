package org.bitbucket.nvxarm.fixpython;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;

class AppTest {
    private static final Logger log = LoggerFactory.getLogger(AppTest.class);

    @Test
    public void main() {
        App.main(new String[]{"src/test/resources/python"});
    }

    @AfterEach
    public void clean() {
        Optional.of("src/test/resources/python/scripts")
                .map(Path::of)
                .map(Path::toFile)
                .map(it -> it.listFiles(file -> StringUtils.endsWithIgnoreCase(file.getName(), ".backup")))
                .map(Arrays::stream)
                .ifPresent(it -> it.forEach(this::restoreBackup));
    }

    private void restoreBackup(File backup) {
        Optional.of(backup)
                .map(File::getPath)
                .map(FilenameUtils::removeExtension)
                .map(FilenameUtils::removeExtension)
                .map(File::new)
                .ifPresent(origin -> restore(backup, origin));
    }

    private void restore(File backup, File origin) {
        try {
            FileUtils.copyFile(backup, origin);
            FileUtils.forceDelete(backup);
            log.info("Restored '{}'", origin.getPath());
        } catch (IOException e) {
            log.warn("Unable to restore '{}'", backup);
            log.error(e.getMessage(), e);
        }
    }
}
